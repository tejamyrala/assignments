
public class BooksDemo {
	public static void main(String[] args) {
		Books b1 = new Books();
		System.out.println(b1.title);
		System.out.println(b1.author);
		System.out.println(b1.price);

		Books b2 = new Books("happiness");
		System.out.println(b2.title);
		System.out.println(b2.author);
		System.out.println(b2.price);

		Books b3 = new Books("happiness", "teja");
		System.out.println(b3.title);
		System.out.println(b3.author);
		System.out.println(b3.price);
	}
}
