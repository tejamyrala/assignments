
public class Sorting {

	public static void main(String[] args) {
				int []v = {3,76,5,87};

		int temp;
		for (int i = 0; i < v.length - 1; i++) {
			for (int j = 0; j < v.length - 1; j++) {
				if (v[j] > v[j + 1]) {
					temp = v[j];
					v[j] = v[j + 1];
					v[j + 1] = temp;
				}
			}
		}
		
		for(int i=0; i<v.length;i++) {
			System.out.print(v[i] + " ");
		}
	}
}
