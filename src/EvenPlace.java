
public class EvenPlace {
	public static void main(String[] args) {
		int i = 56432878;
		int temp = i;
		int left = 0;
		int num = 1;
		int right = 0;
		while (temp > 0) {

			int digit = temp % 10;
			temp = temp / 10;
			if (digit % 2 == 0) {
				right = right * 10 + digit;
				left = digit * num + left;
				num = num * 10;
			}
		}
		System.out.println("INPUT :  " + i);
		System.out.println("LEFT TO RIGHT ORDER :  " + left);
		System.out.println("RIGHT TO LEFT ORDER : " + right);
	}

}
