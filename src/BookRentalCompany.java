import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Objects;

public class BookRentalCompany {
	private static BufferedReader input;

	private static Connection con;
	private static Statement st;
	private static ResultSet rs;

	private static final String JDBC_DRIVER_CLASS;
	private static final String JDBC_URL;
	private static final String DB_USER;
	private static final String DB_PWD;

	static {
		JDBC_DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";
		JDBC_URL = "jdbc:mysql://localhost:3306/bookrentalcompany";
		DB_USER = "root";
		DB_PWD = "root";
		try {
			Class.forName(JDBC_DRIVER_CLASS);

			input = new BufferedReader(new InputStreamReader(System.in));
		} catch (Exception e) {

		}
	}

	public static void main(String[] args) {
		String option = "";

		try {
			con = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PWD);

			while (true) {
				printMenu();

				option = input.readLine();

				switch (option) {
				case "1":
					SearchBooks();
					break;
				case "2":
					AddBooks();
					break;
				case "3":
					UpdateBooks();
					break;
				case "4":
					ReturnBooks();
					break;
				case "5":
					AddBooks();
					break;
				case "6":
					Stastics();
					break;
				case "7":
					return;
				default:
					System.out.println("Invalid input option, please try again");
					input.readLine();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (Objects.nonNull(con)) {
				try {
					con.close();
				} catch (Exception e) {

				}
			}
		}
	}

	private static void Stastics() {
		// TODO Auto-generated method stub
		
	}

	private static void ReturnBooks() {
		// TODO Auto-generated method stub
		
	}

	private static void UpdateBooks() {
		// TODO Auto-generated method stub
		
	}

	private static void AddBooks() {
		// TODO Auto-generated method stub
		
	}

	private static void SearchBooks() {
		
		
	}

	private static void printMenu() {
		
			System.out.println("\n      BOOKS INFORMATION ");
			System.out.println("     ========================");
			System.out.println("1. Search \n2. Add\n3. Update\n4. Return\n5. Issues\n6 Stastics\n7 Return ");

			System.out.print("Your Option: ");
		}

		private static void printLine(char c, int length) {
			for (int i = 0; i < length; i++) {
				System.out.print(c);
			}
			System.out.println();
		}
		
	}



